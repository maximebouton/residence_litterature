Bonjour Sofiane,
Merci pour le dossier que tu m’as envoyé. 
Je l’ai présenté et j'ai expliqué le projet à mes collègues Maxime Bouton et Émile Greis.
Le dossier nous a permis de nous projeter un peu plus précisément dans le projet. Dans la perspective de celui-ci, il faudra que le texte soit plus précis sur les documents convoqués. Cela peut passer par la référence au nom du fichier / dossier correspondant à même le texte. De plus, c’est difficile de se projeter pleinement sans le fichier voix-off qui redit le texte.
Par rapport aux documents iconographiques, beaucoup sont de faible définition. Pour un usage confortable des images, il faudrait qu’elles fassent au moins 2000px de large.
Nous avons ajouté notre devis en pièce jointe. Nous avons pris en compte le délai assez court, la mise en place d’un CMS et la complexité de relier des éléments à un flux temporel et spatial. On te laisse nous faire un retour sur le devis.
Penses-tu qu'il faudra faire une division en trois devis pour avoir ainsi trois bons de commande séparés ? Il faudra en effet que nous puissions faire trois factures séparés.

Voici quelques questions techniques : Quelle est la version du PHP installée sur l’hébergement ? Est-ce possible d’utiliser une base de données MySql ?

Bonne journée,
Maxime, Émile et Vincent